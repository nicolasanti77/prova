//
//  ENCategoria.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ENCategoria : NSObject <NSCoding>

@property (strong, nonatomic) NSString *nome, *descrizione, *ID;
@property (strong, nonatomic) UIColor *coloreEtichetta;

@end
