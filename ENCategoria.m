//
//  ENCategoria.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENCategoria.h"

@implementation ENCategoria

@synthesize nome = _nome, coloreEtichetta = _coloreEtichetta, descrizione = _descrizione, ID = _ID;

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_ID forKey:@"indice"];
    [aCoder encodeObject:_nome forKey:@"nomecategoria"];
    [aCoder encodeObject:_coloreEtichetta forKey:@"coloreetichetta"];
    [aCoder encodeObject:_descrizione forKey:@"descrizione"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self.ID = [aDecoder decodeObjectForKey:@"indice"];
    self.nome = [aDecoder decodeObjectForKey:@"nomecategoria"];
    self.coloreEtichetta = [aDecoder decodeObjectForKey:@"coloreetichetta"];
    self.descrizione = [aDecoder decodeObjectForKey:@"descrizione"];
    return self;
 
}




@end
