//
//  ENControlloreVistaCategorie.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ENSalvaECaricaDati;


@interface ENControlloreVistaElencoCategorie : UITableViewController

@property (strong, nonatomic) ENSalvaECaricaDati *dati;

- (IBAction)eliminaCategoria:(id)sender;


- (IBAction)mettiInModificaLaTabella:(id)sender;



@end
