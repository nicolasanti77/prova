//
//  ENControlloreVistaElencoCategorie.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENControlloreVistaElencoCategorie.h"
#import "ENControlloreVistaElencoTask.h"
#import "ENElencoCategorie.h"
#import "ENSalvaECaricaDati.h"
#import "ENCategoria.h"
#import "ENElencoTask.h"
#import "ENControlloreVistaNuovoTask.h"



@implementation ENControlloreVistaElencoCategorie

@synthesize dati = _dati;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

//Funzione standard che serve a richiamare il controllore quando questi è caricato dal file nib

- (void)awakeFromNib
{
    [super awakeFromNib];
}

// Quando l'applicazione parte suppongo che la prima vista che carica sia stata questa
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    ENSalvaECaricaDati *dati = [[ENSalvaECaricaDati alloc] init];
    
    self.dati = dati;
    
    dati = nil;
    
    
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Azioni lanciate dall'utente





- (IBAction)mettiInModificaLaTabella:(id)sender {

    if (self.tableView.editing == NO){
        [self.tableView setEditing:YES animated:NO];
        [self.tableView reloadData];
    } else {
        [self.tableView setEditing:NO animated:YES];
        [self.tableView reloadData];
    }
   
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    // Per la prima sezione voglio vedere i gruppi di attività "senza categoria" e "Tutte", nella seconda sezione riporto i gruppi per categoria.
    
    if (section == 0) {
        
        return 2;
        
    } else {
        
        return [self.dati.oggettoElencoCategorie.elencoCategorie count];
    }
    

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 //   static NSString *CellIdentifier = @"CellaCategoria";
 //   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //Nella prima sezione della tabella vado a inserire i due gruppi generici  "Nessuna categoria" e "Tutte le attività". Nella sezione successiva vado a inserire i gruppi che rappresentano le varie categorie.
    if (indexPath.section == 1) {
        if (self.tableView.editing == NO) {
            static NSString *CellIdentifier = @"CellaCategoria1";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            ENCategoria *categoria = [self.dati.oggettoElencoCategorie.elencoCategorie objectAtIndex:indexPath.row];
            
            cell.textLabel.text = categoria.nome;
            cell.detailTextLabel.text = categoria.descrizione;
            cell.textLabel.textColor = categoria.coloreEtichetta;
            
            return cell;
            
        } else {
            
            static NSString *CellIdentifier = @"CellaCategoria2";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            ENCategoria *categoria = [self.dati.oggettoElencoCategorie.elencoCategorie objectAtIndex:indexPath.row];
            
            cell.textLabel.text = categoria.nome;
            cell.detailTextLabel.text = categoria.descrizione;
            cell.textLabel.textColor = categoria.coloreEtichetta;
            
            
            return cell;
        }
        
    } else {

            
            static NSString *CellIdentifier = @"CellaCategoria0";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Nessuna categoria";
                    cell.detailTextLabel.text = @"Tutte le attività prive di categoria";
                    
                    cell.textLabel.textColor = [UIColor redColor];
                    break;
                    
                default:
                    cell.textLabel.text = @"Tutte le attività";
                    cell.detailTextLabel.text = @"Tutte le attività indipendentemente dalla categoria";
                    
                    cell.textLabel.textColor = [UIColor greenColor];
                    break;
            }
            
            return cell;

        
    }

}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 0) {
        return NO;
    } else {
        return YES;
    }
        

}

-(UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if (indexPath.row == 0) {
        return UITableViewCellEditingStyleInsert;
    } else {
        return UITableViewCellEditingStyleDelete;

    }
}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        NSLog(@"ciao");
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    //Se l'utente non ha cliccato su di una categoria specifica invio l'elenco delle attività e il tipo di selezione effettuata  
    
    if ([[segue identifier] isEqualToString:@"DaCategorieAAttivitàPrimaSezione"]){
        ENControlloreVistaElencoTask *controlloreVistaElencoTask = [segue destinationViewController];

        int rigaSelezionata = [self.tableView indexPathForSelectedRow].row;
        controlloreVistaElencoTask.elencoTask = self.dati.oggettoElencoTask;
        if (rigaSelezionata == 0) {
            controlloreVistaElencoTask.navigationItem.title = @"Nessuna categoria";
        } else {
            controlloreVistaElencoTask.navigationItem.title = @"Tutte le attività";
        }
        
        
    }
    
    
    //Se l'utente ha cliccato su di una categoria invio l'elenco attività e il nome della categoria selezionata
    
    if ([[segue identifier] isEqualToString:@"DaCategorieAAttivitàSecondaSezione"]){
        ENControlloreVistaElencoTask *controlloreVistaElencoTask = [segue destinationViewController];

        int rigaSelezionata = [self.tableView indexPathForSelectedRow].row;
        ENCategoria *categoria = [self.dati.oggettoElencoCategorie.elencoCategorie objectAtIndex:rigaSelezionata];
        controlloreVistaElencoTask.elencoTask = self.dati.oggettoElencoTask;
        controlloreVistaElencoTask.navigationItem.title = categoria.nome;

    }
    if ([[segue identifier] isEqualToString:@"task"]){
                ENControlloreVistaNuovoTask *controlloreVistaNuovoTask = [segue destinationViewController];
        [controlloreVistaNuovoTask.navigationController.navigationItem setHidesBackButton:NO];
    }
    
}

@end
