//
//  ENControlloreVistaElencoAttività.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ENSalvaECaricaDati, ENElencoTask;

@interface ENControlloreVistaElencoTask : UITableViewController


@property (strong, nonatomic) ENElencoTask *elencoTask;

@property (weak, nonatomic) IBOutlet UITextField *nomeNuovoTask;

- (IBAction)aggiungiTaskSemplice:(id)sender;


@end
