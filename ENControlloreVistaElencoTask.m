//
//  ENControlloreVistaElencoAttività.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENControlloreVistaElencoTask.h"
#import "ENSalvaECaricaDati.h"
#import "ENElencoTask.h"
#import "ENTask.h"
#import "ENControlloreVistaTask.h"


@interface ENControlloreVistaElencoTask()

@property (strong, nonatomic) ENElencoTask *elencoTaskPerCategoria;

@end

@implementation ENControlloreVistaElencoTask


@synthesize elencoTask = _elencoTask, elencoTaskPerCategoria = _elencoTaskPerCategoria;

@synthesize nomeNuovoTask = _nomeNuovoTask;



#pragma mark - Zona di inizializzazione

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inizializzo l'elenco delle attività filtrato per categoria
    self.elencoTaskPerCategoria = [self.elencoTask estraiElencoTaskPerCategoria:self.title]; 

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{

    [self setNomeNuovoTask:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    NSString *s = self.navigationItem.title;
    
    
    int i = [self.elencoTask numeroDiTaskPerCategoria:s];
    
    return i;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellaAttività"];
    

    cell.textLabel.text = [[self.elencoTask estraiTaskConIndice:indexPath.row] nome] ;
    NSString *indice = [[NSString alloc] initWithFormat:@"%i",[self.elencoTask estraiTaskConIndice:indexPath.row].indice];
    cell.detailTextLabel.text = indice;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"DaElencoAAttività"]) {
    
        
    

    ENControlloreVistaTask *controlloreVistaTask = [[ENControlloreVistaTask alloc] init];
    controlloreVistaTask = [segue destinationViewController];
    ENTask *task = [ENTask alloc];
    task = [self.elencoTask estraiTaskConIndice:[self.tableView indexPathForSelectedRow].row];
    controlloreVistaTask.task = task;
    }
    
}

#pragma mark - Funzioni di controllo

- (IBAction)aggiungiTaskSemplice:(id)sender {
    
    [self.elencoTask inserisciNuovoTaskDaNome:self.nomeNuovoTask.text];
    [sender resignFirstResponder];
    
}
@end
