//
//  ENControlloreVistaTaskViewController.h
//  Entropia
//
//  Created by Nicola Santi on 12/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENTask.h"

@interface ENControlloreVistaTask : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *indice;
@property (weak, nonatomic) IBOutlet UILabel *nome;
@property (weak, nonatomic) IBOutlet UILabel *dataInizio;
@property (weak, nonatomic) IBOutlet UILabel *dataFine;
@property (weak, nonatomic) IBOutlet UILabel *categorie;
@property (weak, nonatomic) IBOutlet UITextView *nota;
@property (weak, nonatomic) IBOutlet UILabel *dataOraAvviso;
@property (weak, nonatomic) UITableViewCell *cellaAvviso;


@property (strong, nonatomic) ENTask *task;



- (IBAction)aggiungi:(id)sender;

@end
