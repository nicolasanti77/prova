//
//  ENControlloreVistaTaskViewController.m
//  Entropia
//
//  Created by Nicola Santi on 12/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENControlloreVistaTask.h"
#import "ENCategoria.h"

@interface ENControlloreVistaTask ()

@end

@implementation ENControlloreVistaTask
@synthesize indice = _indice;
@synthesize nome = _nome;
@synthesize dataInizio = _dataInizio;
@synthesize dataFine = _dataFine;
@synthesize categorie = _categorie;
@synthesize nota = _nota;
@synthesize dataOraAvviso = _dataOraAvviso;
@synthesize task = _task;
@synthesize cellaAvviso = _cellaAvviso;




- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    ENTask *task = [ENTask alloc];
    task = self.task;
    self.indice.text = [NSString stringWithFormat:@"%i", task.indice];
    self.nome.text = task.nome;
    self.nota.text = task.nota;
    NSDateFormatter *formatoData = [[NSDateFormatter alloc] init];
    [formatoData setTimeStyle:NSDateFormatterMediumStyle];
    [formatoData setDateStyle:NSDateFormatterMediumStyle];
    
    self.dataFine.text = [formatoData stringFromDate:task.dataFine];
    //una prova da eliminare
    NSString *prova = [[NSString alloc] initWithString:[formatoData stringFromDate:task.dataFine]];
    NSLog(@"%@", prova);
    //
    self.dataInizio.text = [formatoData stringFromDate:task.dataInizio];
    self.dataOraAvviso.text = [formatoData stringFromDate:task.dataOraAvviso];
    if (task.categorie) {
        for (ENCategoria *cat in task.categorie) {
            
            self.categorie.text = [NSString stringWithFormat:@"%@, %@", self.categorie.text, cat.nome];
        }
    }
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [self setIndice:nil];
    [self setNome:nil];
    [self setDataInizio:nil];
    [self setDataFine:nil];
    [self setCategorie:nil];
    [self setNota:nil];
    [self setTask:nil];
    [self setDataOraAvviso:nil];
    [self setNota:nil];
    [self setCellaAvviso:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
      *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}



- (IBAction)aggiungi:(id)sender {
    
  /*  
    NSIndexPath *indice = [NSIndexPath indexPathForRow:1 inSection:1];
    NSArray *array = [[NSArray alloc] initWithObjects:indice, nil];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationTop];
    [self.tableView endUpdates];
  */ 
}




@end
