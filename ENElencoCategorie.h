//
//  ENElencoCategorie.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENProtocolloDiSalvataggio.h"
@class ENCategoria;

@interface ENElencoCategorie : NSObject <NSCoding>

@property (weak, nonatomic) id <DelegatoAlSalvataggio> delegato;
@property (strong, nonatomic) NSMutableArray *elencoCategorie;
@property (readwrite, nonatomic) int ultimoIndice;

-(id)initConDelegato:(id) delegato;
-(void)AddCategoriaAElenco:(ENCategoria *) categoria;

@end
