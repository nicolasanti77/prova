//
//  ENElencoCategorie.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENElencoCategorie.h"
#import "ENCategoria.h"


@implementation ENElencoCategorie

@synthesize elencoCategorie = _elencoCategorie, delegato = _delegato, ultimoIndice = _ultimoIndice;

//Inizializzo in maniera fittizia l'oggetto per passarlo alla vista Elenco Categorie

-(id)initConDelegato:(id)delegato{
    
    self = [super init];
    self.delegato = delegato;
    if (self) {
        

        
        self.elencoCategorie = [[NSMutableArray alloc] init];

        for (int i = 1; i <= 10; i++) {
            ENCategoria *categoria = [[ENCategoria alloc] init];
            //NSString *prova = [[NSString alloc] initWithFormat:@"Nome %i",i];
            UIColor *colore = [[UIColor alloc] initWithRed:(0.5) green:(0.5) blue:(0.5) alpha:(1)];
            categoria.ID = [[NSString alloc] initWithFormat:@"%i",i];
            categoria.nome = [[NSString alloc] initWithFormat:@"Nome Categoria %i",i];
            categoria.descrizione = [[NSString alloc] initWithFormat:@"Descrizione Categoria %i",i];
            categoria.coloreEtichetta = colore;
            
            [self.elencoCategorie addObject:categoria];
            
            
        }
        /* stavo definendo una categoria nulla ma secondo me faccio male da eliminare
        ENCategoria *catNulla = [[ENCategoria alloc] init];
        UIColor *colore = [UIColor redColor];
        catNulla.nome = @"Senza Categorie";
        catNulla.coloreEtichetta = colore;
        catNulla.descrizione = @"Nessuna categoria"
         */
         

        
    }
    [self.delegato salvaElencoCategorie:self];
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_elencoCategorie forKey:@"elencocategorie"];
    
    //[aCoder encodeObject:_delegato forKey:@"delegato"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    return self;
}

-(void)AddCategoriaAElenco:(ENCategoria *)categoria{
    
    
    
}

@end
