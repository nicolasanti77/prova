//
//  ENElencoTask.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENProtocolloDiSalvataggio.h"
@class ENTask, ENCategoria;

@interface ENElencoTask: NSObject <NSCoding>

@property (weak, nonatomic) id <DelegatoAlSalvataggio> delegato;
@property (strong, nonatomic) NSMutableArray *elencoTask;
@property (readwrite, nonatomic) int ultimoIndice;

-(id) initConDelegato:(id) delegato;

-(int) numeroDiTaskPerCategoria:(NSString *) categoria;

-(ENElencoTask *) estraiElencoTaskPerCategoria: (NSString *) nomeCategoria;

-(ENTask *) estraiTaskConIndice: (int) indice;

-(void) inserisciNuovoTaskDaNome: (NSString *) nomeTask;




@end
