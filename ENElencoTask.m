//
//  ENElencoTask.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENElencoTask.h"
#import "ENTask.h"
#import "ENCategoria.h"

@implementation ENElencoTask

@synthesize elencoTask = _elencoTask, delegato = _delegato, ultimoIndice = _ultimoIndice;




#pragma mark - Metodi di inizializzazione


-(id)initConDelegato:(id) delegato{
    
    self = [super init];
    self.delegato = delegato;
    if (self) {
        //Definisco un attività
        for (int i = 1; i <= 10; i++) {
            
            ENTask *task = [[ENTask alloc] init];
            task.indice = i;
            self.ultimoIndice = i;
            NSString *stringa = [[NSString alloc] initWithFormat:@"nicola %i",i];
            NSDate *data = [[NSDate alloc] init];
            task.dataFine = data;
            task.dataInizio = [data dateByAddingTimeInterval:100000];
            task.dataOraAvviso = [data dateByAddingTimeInterval:100000000];
            task.nome = stringa;
            task.nota =@"Questa vista la elimino non ti preoccupare";

            ENCategoria *categoria = [[ENCategoria alloc] init];
            if (i<4) {
                categoria.nome = @"Nome Categoria 1";

            }
            if (i>=4) {
                categoria.nome = @"Nome Categoria 2";

            }
            if (i>7) {
               
                categoria.nome = nil;

            
            }
            
            NSMutableSet *categorie = [[NSMutableSet alloc] initWithObjects:categoria, nil];
            
            task.categorie = categorie;
            
            // Aggiungo l'attività sopra costruita al mio elenco di attività
            if (!self.elencoTask){
                NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:task, nil];
                self.elencoTask = array;
            } else {
                [self.elencoTask addObject:task];
            }


            task = nil;
            categorie = nil;

        }
    }
    
    //Chiamo la funzione del delegato per salvare su file
    
    [self.delegato salvaElencoTask:self];

    
    return self;
    
}


 


#pragma mark - Metodi di interrogazione


-(int)numeroDiTaskPerCategoria:(NSString *)nomeCategoria{
    
    int numeroTaskPerCategoria = 0;
//Se voglio tutte le attività restituisco il numero completo di attività    
    if ([nomeCategoria isEqualToString:@"Tutte le attività"]) {
        numeroTaskPerCategoria = [self.elencoTask count];
    } else {
        
        for (ENTask *task in self.elencoTask) {
            for (ENCategoria *cat in task.categorie) {
                //Se voglio le attività senza nessuna categoria interrogo quali attività hanno a Nil il nome della categoria
                if ([nomeCategoria isEqualToString:@"Nessuna categoria"]) {
                    if (cat.nome == nil) {
                        ++numeroTaskPerCategoria;
                        break;
                    } 
                    //Se arrivo qui vulo dire che chiedo il numero di attività che contengono quella specifica categoria
                } else  {
                    if ([cat.nome isEqualToString:nomeCategoria]) {
                        
                        
                        ++numeroTaskPerCategoria;
                        break;
                    }
                }
                
            }
        }
        
        
    }
    return numeroTaskPerCategoria;
}


 
-(ENElencoTask *)estraiElencoTaskPerCategoria:(NSString *)nomeCategoria{
    

    
    ENElencoTask *elencoTaskConCategoria = [[ENElencoTask alloc] init];

    for (ENTask *task in self.elencoTask) {
        for (ENCategoria *categoria in task.categorie) {
            
                if ([categoria.nome isEqualToString:nomeCategoria]) {
                    
                
            }
        }
    }
   
    return elencoTaskConCategoria;
}


 
-(ENTask *) estraiTaskConIndice:(int)indice{
    
    return [self.elencoTask objectAtIndex:indice];
    
}



#pragma mark - Metodi di inserimento


-(void)inserisciNuovoTaskDaNome:(NSString *)nomeTask{
    
   
   
    NSLog(@"%i",self.ultimoIndice);
    
    ENTask *nuovoTask = [[ENTask alloc] init];
    
    nuovoTask.indice = ++self.ultimoIndice;
    nuovoTask.nome = nomeTask;
    
    
    [self.elencoTask addObject:nuovoTask];
    
    
    NSLog(@"%i",self.ultimoIndice); 

   
}



#pragma mark - Protocollo NScoding


-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_elencoTask forKey:@"Elenco Attività"];
    
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    return self;    
}











@end
