//
//  ENProtocolloDiSalvataggio.h
//  Entropia
//
//  Created by Nicola Santi on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ENElencoTask, ENElencoCategorie;

@protocol DelegatoAlSalvataggio <NSObject>

-(void) salvaElencoTask:(ENElencoTask *) tasks;

-(void) salvaElencoCategorie:(ENElencoCategorie *) categorie;



@end
