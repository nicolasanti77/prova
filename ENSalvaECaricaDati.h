//
//  ENSalvaECaricaDati.h
//  Entropia
//
//  Created by Nicola Santi on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//  Primo commit di nicola
// Secondo commit

#import <Foundation/Foundation.h>
#import "ENProtocolloDiSalvataggio.h"
@class ENElencoCategorie, ENElencoTask;


@interface ENSalvaECaricaDati : NSObject <DelegatoAlSalvataggio>

@property (retain,nonatomic) ENElencoTask *oggettoElencoTask;
@property (retain,nonatomic) ENElencoCategorie *oggettoElencoCategorie;



@end
