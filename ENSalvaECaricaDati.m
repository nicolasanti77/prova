//
//  ENSalvaECaricaDati.m
//  Entropia
//
//  Created by Nicola Santi on 04/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENSalvaECaricaDati.h"
#import "ENElencoTask.h"
#import "ENElencoCategorie.h"

@interface ENSalvaECaricaDati();

@property (strong, nonatomic) NSString *percorsoFileTask, *percorsoFileCategorie;


@end


@implementation ENSalvaECaricaDati

@synthesize oggettoElencoTask = _oggettoElencoTask, oggettoElencoCategorie = _oggettoElencoCategorie;
@synthesize percorsoFileCategorie = _percorsoFileCategorie, percorsoFileTask =_percorsoFileTask;

// Inizializzo la base dei dati leggendo dai file o creandone dei nuovi
-(id)init{
    
    //  Inizializzo la variabile con il percorso del file con i dati salvati in precedenza
    
    NSArray *percorsi = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDirectory, YES);
    self.percorsoFileTask = [NSString stringWithFormat:@"%@/%@",[percorsi objectAtIndex:0],@"task_00.00.001.plist"];
    self.percorsoFileCategorie = [NSString stringWithFormat:@"%@/%@",[percorsi objectAtIndex:0],@"categorie_00.00.001.plist"];
    
    NSFileManager *appoggio = [[NSFileManager alloc] init];
    
    
    //  Verifico se il file delle attività esiste (potrebbe essere la prima volta che si lancia l'applicazione)
    if ([appoggio fileExistsAtPath:_percorsoFileTask]){
        
        
        //  Assegno il contenuto del file alla proprietà che conterrà tutte le attività
        self.oggettoElencoTask = (ENElencoTask *) [[[NSArray alloc] initWithContentsOfFile:_percorsoFileTask] objectAtIndex:0];
    }
    
    
    //  Verifico se il file delle categorie esiste (potrebbe essere la prima volta che si lancia l'applicazione)
    if ([appoggio fileExistsAtPath:_percorsoFileTask]){
        
        
        //  Assegno il contenuto del file alla proprietà che conterrà tutte le categorie
        self.oggettoElencoCategorie = (ENElencoCategorie *) [[[NSArray alloc] initWithContentsOfFile:_percorsoFileCategorie] objectAtIndex:0];
    }
    
    
    //  Se il file delle attività non esisteva o era vuoto devo inizializzarlo
    if ([self.oggettoElencoTask.elencoTask count] == 0) {
        
        
        
        //Inizializzo un oggetto ENElencoTask e lo salvo automaticamente
        self.oggettoElencoTask = (ENElencoTask *) [[ENElencoTask alloc] initConDelegato:self];

    }
    
    
    //  Se il file delle categorie non esisteva o era vuoto devo inizializzarlo
    if ([self.oggettoElencoCategorie.elencoCategorie count] == 0) {
        
        
        //Inizializzo un oggetto ENElencoTask e lo salvo automaticamente
        self.oggettoElencoCategorie = (ENElencoCategorie *) [[ENElencoCategorie alloc] initConDelegato:self]; 
        
    }
    
    //  Verifico che la consistenza dei dati nel file sia corretta c'è da fare di più ma per il momento basta
/*
    for (NSMutableDictionary * sezione in listaDiAttivita) {
        
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[sezione objectForKey:@"Attività"]];
        for (NSMutableArray * riga in array) {
            
            if (riga.count==1) {
                [riga addObject:@""];
                [riga addObject:@""];
                
            }
            
            
        }
    }
*/    
    return self;    
}



//Metodi che conformano ENSalvaECaricaDati al protocollo di salvataggio da me creato
-(void)salvaElencoTask:(ENElencoTask *)tasks{
    
    
    //Archivio le attività
    [NSKeyedArchiver archiveRootObject:tasks toFile:self.percorsoFileTask];
    
}


//Metodi che conformano ENSalvaECaricaDati al protocollo di salvataggio da me creato
-(void)salvaElencoCategorie:(ENElencoCategorie *)categorie{
    
    //Archivio le categorie
    [NSKeyedArchiver archiveRootObject:categorie toFile:self.percorsoFileCategorie];

}




@end
