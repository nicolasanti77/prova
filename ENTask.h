//
//  ENAttività.h
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ENTask : NSObject <NSCoding> 

@property (readwrite, nonatomic) int indice;
@property (readwrite, nonatomic) NSString *nome;
@property (readwrite, nonatomic) NSDate *dataInizio, *dataFine, *dataOraAvviso;
@property (readwrite, nonatomic) NSString *nota;
@property (readwrite, nonatomic) NSMutableSet *categorie;

- (id) initOggettoTask:(int)indice nome:(NSString *) nome;

/* probabilmente inutile da eliminare prossimamente
- (id) initOggettoTask:(int)indice 
                  nome:(NSString *) nome 
             categorie:(NSMutableSet *) categorie
dataInizio:(NSDate *) dataInizio
dataFine:(NSDate *) dataFine
dataOraAvviso:(NSdate *) dataOraAvviso;
*/

- (BOOL) aggiungiCategoria:(NSString *) categoria;

- (BOOL) eliminaCategoria:(NSString *) categoria;


@end
