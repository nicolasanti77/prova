//
//  ENAttività.m
//  Entropia
//
//  Created by Nicola Santi on 02/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ENTask.h"

@implementation ENTask

@synthesize categorie = _categorie, dataFine = _dataFine, dataInizio = _dataInizio, dataOraAvviso = _dataOraAvviso, indice = _indice, nome = _nome, nota = _nota;


#pragma mark - Inizializzazione oggetto

//Da eliminare perchè inutule usata per iniziare a fare delle prove...
- (id) initOggettoTask:(int) indice nome:(NSString *) nome{
    
    self = [super init];
    
    if (self){
        _nome = nome;
        _indice = indice;
        _categorie = nil;
        _dataFine = nil;
        _dataInizio = nil;
        _dataOraAvviso = nil;
        
        return self;
        
    }
    return nil;
}

- (id) init{
    
    if (!self) {
        self = [super init];
    }
    
    return self;
}

#pragma mark - Gestione oggetto

//La funzione restituisce "YES" se la categoria c'erà già o è "Nil" la categoria passata "NO" se non c'era.
- (BOOL) aggiungiCategoria:(NSString *)categoria{
    
    if (categoria) {
        if (!_categorie) {
            NSMutableSet *cat = [[NSMutableSet alloc] initWithObjects:categoria, nil];
            _categorie = cat;
            return NO;
        }
    } else {
        if ([_categorie containsObject:categoria]) {
            return YES;
        } else {
            [_categorie addObject:categoria];
            return NO;
        }
        
        
    }
    return YES;
}

//La funzione restituisce "YES" se ha eliminato la categoria che è stata passata e "NO" se per qualunque motivo non ha eliminato la categoria
-(BOOL)eliminaCategoria:(NSString *)categoria{
    
    if (_categorie) {
        if ([_categorie containsObject:categoria]) {
            
            [_categorie removeObject:categoria];
            if ([_categorie count] == 0) {
            _categorie = nil;
            }
            return YES;
            
        } else {
            
            return NO;
        }
        
    }
    
    return NO;
    
}



#pragma mark - Protocollo NSCoding

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_nome forKey:@"nomeattività"];
    [aCoder encodeInt:_indice forKey:@"indice"];
    [aCoder encodeObject:_categorie forKey:@"categorie"];
    [aCoder encodeObject:_dataFine forKey:@"datafineattività"];
    [aCoder encodeObject:_dataInizio forKey:@"datainizioattività"];
    [aCoder encodeObject:_dataOraAvviso forKey:@"dataoraAvvisoattività"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    return self;
    
}

@end
