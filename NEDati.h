//
//  NEDati.h
//  Nemo
//
//  Created by Nicola Santi on 09/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Attenzione NEDati da eliminare in quanto non fa parte di questo progetto

#import <Foundation/Foundation.h>

@interface NEDati : NSObject {
    
    NSMutableArray *listaDiAttivita;
    NSString *percorsoFile;
    NSMutableArray *datiAttivita;
    
}

@property (retain,nonatomic) NSString *percorsoFile;
@property (retain, readwrite) NSMutableArray *listaDiAttivita;
@property (retain, readwrite) NSMutableArray *datiAttivita;



-(void) inserisciDati: (NSMutableArray *) titoloAttivita  sezione: (int) sezione;
-(void) sostituisciDati: (NSMutableArray *) titoloAttivita posizione: (NSIndexPath *) posizione;
-(void) eliminaDati: (NSIndexPath *) posizione;

@end
