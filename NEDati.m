//
//  NEDati.m
//  Nemo
//
//  Created by Nicola Santi on 09/01/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Attenzione NEDati da eliminare in quanto non fa parte di questo progetto

#import "NEDati.h"

@implementation NEDati

@synthesize percorsoFile, listaDiAttivita, datiAttivita;





-(id)init{
    
    //  Inizializzo la variabile con il percorso del file con i dati salvati in precedenza
    
    NSArray *percorsi = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDirectory, YES);
    self.percorsoFile = [NSString stringWithFormat:@"%@/%@",[percorsi objectAtIndex:0],@"data_00.00.003.plist"];
    
    NSFileManager *appoggio = [[NSFileManager alloc] init];
    
    
    //  Verifico se il file esiste (potrebbe essere la prima volta che si lancia l'applicazione)
    if ([appoggio fileExistsAtPath:percorsoFile]){
        
        
        //  Creo un Array che contenga gli oggetti Dictionary (due liste di attività) e lo inizializzo con il contenuto di un file compilato da istanze precedenti dell'applicazione (ho da inserimenti manuali da computer!)
        self.listaDiAttivita = [[NSMutableArray alloc] initWithContentsOfFile:percorsoFile];
    }
    
    //  Se il file non esisteva o era vuoto devo inizializzarlo
    if ([listaDiAttivita count] == 0) {
        
        
        
        //Creo un Array he contanga la lista delle attività da fare
        listaDiAttivita = [[NSMutableArray alloc] init];
        
        //Creo un oggetto dizionario che contiene la lista delle attività da fare
        NSDictionary *daFare = [NSDictionary dictionaryWithObject:[NSMutableArray array] forKey:@"Attività"];
        
        //Creao un oggetto dizionario che contenga le attività fatte
        NSDictionary *attivitaFatte = [NSDictionary dictionaryWithObject:[NSMutableArray array] forKey:@"Attività"];
        
        //Aggiungo alla listaDiAttività i due dizionari appena creti
        
        [listaDiAttivita addObject:daFare];
        [listaDiAttivita addObject:attivitaFatte];
        [listaDiAttivita writeToFile:percorsoFile atomically:YES];
    }

      
        //  Verifico che la consistenza dei dati nel file sia corretta c'è da fare di più ma per il momento basta
        for (NSMutableDictionary * sezione in listaDiAttivita) {

            NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[sezione objectForKey:@"Attività"]];
            for (NSMutableArray * riga in array) {
                
                if (riga.count==1) {
                    [riga addObject:@""];
                    [riga addObject:@""];
                    
                }
    

            }
        }
    
    return self;    
}



-(void)inserisciDati:(NSMutableArray *)dati sezione:(int) sezione{
        
    [[[listaDiAttivita objectAtIndex:sezione] objectForKey:@"Attività"] addObject:dati];
    [listaDiAttivita writeToFile:percorsoFile atomically:YES];
}


-(void)sostituisciDati:(NSMutableArray *)dati posizione:(NSIndexPath *)posizione{
    
    [[[listaDiAttivita objectAtIndex:posizione.section] objectForKey:@"Attività"] replaceObjectAtIndex:posizione.row withObject:dati];
    [listaDiAttivita writeToFile:percorsoFile atomically:YES];
}

-(void)eliminaDati:(NSIndexPath *)posizione{
    
    [[[listaDiAttivita objectAtIndex:posizione.section] objectForKey:@"Attività"] removeObjectAtIndex:posizione.row];
    [listaDiAttivita writeToFile:percorsoFile atomically:YES];
    
}



@end
